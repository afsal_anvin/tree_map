import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:treemap/src/tiles/squarify.dart';
import 'package:treemap/src/tiles/tile.dart';
import 'package:treemap/src/treemap.dart';
import 'package:treemap/src/treenode.dart';

class TreeMapLayout extends StatelessWidget {
  final TreeNode root;
  final Tile tile;
  final bool round;

  TreeMapLayout({
    this.root,
    this.tile = const Squarify(),
    this.round = false,
  });
  // final currrencyFormat = new NumberFormat("#,##0.00", "en_US");
  // String convertToAmountFormat(String amount) {
  //   return amount != null
  //       ? currrencyFormat
  //           .format(double.parse((amount).replaceAll(',', '')))
  //           .toString()
  //       : "";
  // }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        var treemap = TreeMap(
          root: root,
          size: Size(constraints.maxWidth, constraints.maxHeight),
          tile: tile,
        );

        return Stack(
          children: treemap.leaves.fold([], (result, node) {
            return result
              ..add(
                Positioned(
                  top: node.top,
                  left: node.left,
                  width: node.right - node.left,
                  height: node.bottom - node.top,
                  child: node.builder != null
                      ? node.builder(context)
                      : GestureDetector(
                          onTap: node.onTap,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                // AutoSizeText(
                                //   node.text == null ? "" : node.text,
                                //   minFontSize: 2,
                                //   maxLines: 1,
                                //   textAlign: TextAlign.center,
                                //   style: node.textStyle,
                                // ),
                                AutoSizeText(
                                  node.value == null
                                      ? ""
                                      : node.convertFunction(
                                          node.value.toString()),
                                  minFontSize: 2,
                                  maxLines: 1,
                                  //textAlign: TextAlign.center,
                                  style: node.textStyle,
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                              color: node.color,
                              border: Border.all(
                                width: 1,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                ),
              );
          }),
        );
      },
    );
  }
}

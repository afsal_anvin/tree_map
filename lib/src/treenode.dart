import 'package:flutter/material.dart';
import 'package:treemap/src/treenode_base.dart';

class TreeNode implements TreeNodeBase {
  List<TreeNode> _children;
  EdgeInsets _margin;
  EdgeInsets _padding;
  num _value;
  String _text;
  Color _color;
  Function _convertFunction;
  Function _onTap;
  TextStyle _textStyle;
  WidgetBuilder _builder;
  TreeNode _parent;
  double top = 0;
  double left = 0;
  double right = 0;
  double bottom = 0;

  List<TreeNode> get children => _children;
  num get value => _value;
  String get text => _text;
  Color get color => _color;
  Function get convertFunction => _convertFunction;
  Function get onTap => _onTap;
  TextStyle get textStyle => _textStyle;
  EdgeInsets get margin => _margin;
  EdgeInsets get padding => _padding;
  WidgetBuilder get builder => _builder;
  TreeNode get parent => _parent;

  TreeNode.node({
    @required List<TreeNode> children,
    EdgeInsets margin = const EdgeInsets.all(0),
    EdgeInsets padding = const EdgeInsets.all(0),
  })  : _children = children,
        _margin = margin,
        _padding = padding,
        assert(children != null && children.length > 0) {
    _value = 0;
    _text = "";
    _color = null;
    _onTap = () {};
    _textStyle = TextStyle(color: Colors.black);
    for (var child in children) {
      _value += child.value;
      _text = child.text;
      _color = child.color;
      _textStyle = child.textStyle;
      _convertFunction = child._convertFunction;
      _onTap = child.onTap;
      child._parent = this;
    }
  }

  TreeNode.leaf({
    @required num value,
    @required String text,
    @required Color color,
    @required TextStyle textStyle,
    @required Function convertFunction,
    @required Function onTap,
    WidgetBuilder builder,
    EdgeInsets margin = const EdgeInsets.all(0),
  })  : _value = value,
        _text = text,
        _color = color,
        _textStyle = textStyle,
        _convertFunction = convertFunction,
        _onTap = onTap,
        _builder = builder,
        _margin = margin,
        assert(value != null);

  int get depth {
    int depth = 0;
    TreeNode parent = _parent;
    while (parent != null) {
      parent = parent._parent;
      depth++;
    }
    return depth;
  }

  List<TreeNode> get leaves {
    var leafs = List<TreeNode>();
    for (var child in children) {
      if (child.children == null) {
        leafs.add(child);
      } else {
        leafs.addAll(child.leaves);
      }
    }
    return leafs;
  }

  eachBefore(Function(TreeNode) callback) {
    var node = this;
    var nodes = [node];

    while (nodes.isNotEmpty && (node = nodes.removeLast()) != null) {
      callback(node);
      var children = node.children;
      if (children != null) {
        nodes.addAll(children);
      }
    }
  }
}

import 'package:flutter/material.dart';
import 'package:treemap/treemap.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  String noConvertAmountFormat(String amount) {
    return amount;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.red,
          width: 425,
          //height: 424,
          child: TreeMapLayout(
            tile: Binary(),
            root: TreeNode.node(
                // padding: EdgeInsets.symmetric(horizontal: 20),
                children: [
                  TreeNode.leaf(
                      value: 8989876,
                      text: "Company 1",
                      margin: EdgeInsets.all(5),
                      convertFunction: noConvertAmountFormat,
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 98989,
                      text: "Company 2",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 868686,
                      text: "Company 3",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 879879,
                      text: "Company 1",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 78687,
                      text: "Company 2",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 7687687,
                      text: "Company 3",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 767676,
                      convertFunction: noConvertAmountFormat,
                      text: "Company 1",
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 3434343,
                      text: "Company 2",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                  TreeNode.leaf(
                      value: 434343,
                      text: "Company 3",
                      convertFunction: noConvertAmountFormat,
                      margin: EdgeInsets.all(5),
                      onTap: () {
                        print("object");
                      }),
                ]),
          ),
        ),
      ),
    );
  }
}
